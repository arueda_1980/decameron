<?php

namespace AppBundle\Entity;

/**
 * Habitacion
 */
class Habitacion
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $tipo;

    /**
     * @var string
     */
    private $acomodacion;

    /**
     * @var \AppBundle\Entity\Hotel
     */
    private $hotel;


    /**
     * @var integer
     */
    private $cantidad;
		
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return Habitacion
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set acomodacion
     *
     * @param string $acomodacion
     *
     * @return Habitacion
     */
    public function setAcomodacion($acomodacion)
    {
        $this->acomodacion = $acomodacion;

        return $this;
    }

    /**
     * Get acomodacion
     *
     * @return string
     */
    public function getAcomodacion()
    {
        return $this->acomodacion;
    }

    /**
     * Set hotel
     *
     * @param \AppBundle\Entity\Hotel $hotel
     *
     * @return Habitacion
     */
    public function setHotel(\AppBundle\Entity\Hotel $hotel = null)
    {
        $this->hotel = $hotel;

        return $this;
    }

    /**
     * Get hotel
     *
     * @return \AppBundle\Entity\Hotel
     */
    public function getHotel()
    {
        return $this->hotel;
    }
	
    /**
     * Get cantidad
     *
     * @return integer
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }
	
    /**
     * Set cantidad
     *
     * @param integer $cantidad
     *
     * @return Habitacion
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }
}

