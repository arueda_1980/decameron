<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\Hotel;
use AppBundle\Entity\Habitacion;

class DefaultController extends Controller
{
    /**
    * @Route("/home", name="homepage")
	* @Method({"GET", "POST"})  
    */
    public function indexAction(Request $request)
    {
		$guardarHotel = $request->request->get('guardarHotel', 0);
		$valorNombre = $request->request->get('valorNombre', 0);
		$valorDir = $request->request->get('valorDir', 0);
		$valorCiudad = $request->request->get('valorCiudad', 0);
		$valorNit = $request->request->get('valorNit', 0);
		$valorHabitaciones = $request->request->get('valorHabitaciones', 0);
		
		if(trim($guardarHotel)=="1")
		 {
			$hotelValid = $this->getDoctrine()->getRepository('AppBundle:Hotel')->findOneBy(array('nombre' => $valorNombre));
			if(count($hotelValid)==0)
			 {
				$registro = new Hotel();
				$registro->setNombre($valorNombre);
				$registro->setDireccion($valorDir);
				$registro->setCiudad($valorCiudad);
				$registro->setNit($valorNit);
				$registro->setHabitaciones($valorHabitaciones);
				$em = $this->getDoctrine()->getEntityManager();
				$em->persist($registro);
				$em->flush();
			 }
		 }

		$hotel = $this->getDoctrine()->getRepository('AppBundle:Hotel')->findAll();
        return $this->render('default/index.html.twig', array(
                    'p1' => $guardarHotel,
                    'listHotel' => $hotel,
        ));
    }
	
    /**
    * @Route("/place", name="place")
	* @Method({"GET", "POST"})  
    */
    public function placeAction(Request $request)
    {
		$valorNombre = $request->query->get('name','');
		$valorId = $request->query->get('hotel','');
		$guardarHotelSede = $request->request->get('guardarHotelSede', 0);
		$valorTipo = $request->request->get('valorTipo', 0);
		$valorCantidad = $request->request->get('valorCantidad', 0);
		$valorAcomodacion = $request->request->get('valorAcomodacion', 0);
		$hotel = $this->getDoctrine()->getRepository('AppBundle:Hotel')->find($valorId);
		
		if(trim($guardarHotelSede)=="1")
		 {
			 $habitacionValid = $this->getDoctrine()->getRepository('AppBundle:Habitacion')->findOneBy(array('tipo' => $valorTipo,'acomodacion' => $valorAcomodacion,'hotel' => $hotel));
			 if(count($habitacionValid)==0)
			  {
				$registro = new Habitacion();
				$registro->setTipo($valorTipo);
				$registro->setAcomodacion($valorAcomodacion);
				$registro->setCantidad($valorCantidad);
				$registro->setHotel($hotel);
				$em = $this->getDoctrine()->getEntityManager();
				$em->persist($registro);
				$em->flush();
			  }
		 }
		
		$sede = $this->getDoctrine()->getRepository('AppBundle:Habitacion')->findBy(array('hotel' => $hotel));
        return $this->render('default/place.html.twig', array(
                    'valorNombre' => $valorNombre,
                    'valorId' => $valorId,
                    'listSede' => $sede,
        ));
    }
}
